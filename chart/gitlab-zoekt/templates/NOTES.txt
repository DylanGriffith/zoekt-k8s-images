1. Index a repo:
   ```
   kubectl exec zoekt-0 -- apk add curl
   kubectl exec zoekt-0 -- curl -XPOST -d '{"CloneUrl":"https://gitlab.com/gitlab-org/gitlab-development-kit.git","RepoId":74823}' 'http://127.0.0.1:6060/index'
   ```
2. Search:
   ```
   kubectl exec zoekt-0 -- apk add curl
   kubectl exec zoekt-0 -- curl -XPOST -d '{"Q":"gitaly"}' 'http://127.0.0.1:6070/api/search'
   ```
3. Services are exposed for port 6060 (indexserver), 6070 (webserver) for each replica (where the replica number is part of the DNS name) so you can connect from any other pod to index and search:
   ```
   kubectl exec <some-pod> -- curl -XPOST -d '{"Q":"gitaly"}' 'http://zoekt-0.zoekt.default.svc.cluster.local:6070/api/search'
   ```
